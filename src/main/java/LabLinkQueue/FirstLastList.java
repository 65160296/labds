/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabLinkQueue;

import LabLinkStack.*;

/**
 *
 * @author Tncpop
 */
class FirstLastList {
    private Link first; // Reference to the first item
    private Link last; // Reference to the last item

    public FirstLastList() {
        first = null; // No items on the list yet
        last = null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void insertLast(long dd) {
        Link newLink = new Link(dd); // Create a new link
        if (isEmpty()) {
            first = newLink; // first --> newLink
        } else {
            last.next = newLink; // old last --> newLink
        }
        last = newLink; // newLink <-- last
    }

    public long deleteFirst() {
        long temp = first.dData;
        if (first.next == null) {
            last = null;
        }
        first = first.next;
        return temp;
    }

    public void displayList() {
        Link current = first; // Start at the beginning
        while (current != null) {
            current.displayLink(); // Print data
            current = current.next; // Move to the next link
        }
        System.out.println();
    }
}


