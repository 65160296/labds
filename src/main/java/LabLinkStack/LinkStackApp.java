/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabLinkStack;

/**
 *
 * @author Tncpop
 */
public class LinkStackApp {
    public static void main(String[] args) {
        LinkStack theStack = new LinkStack(); // สร้างสแต็ก

        theStack.push(20); // ใส่ข้อมูลลงในสแต็ก
        theStack.push(40);
        theStack.displayStack(); // แสดงสถานะของสแต็ก
        theStack.push(60); // ใส่ข้อมูลลงในสแต็ก
        theStack.push(80);
        theStack.displayStack(); // แสดงสถานะของสแต็ก
        theStack.pop(); // ดึงข้อมูลออกจากสแต็ก
        theStack.pop();
        theStack.displayStack(); // แสดงสถานะของสแต็ก
    }
}